package handleBigNum;

import java.util.logging.Logger;

public class MyBigNumber {
	
	private static final Logger logger = Logger.getLogger(MyBigNumber.class.getName());
	
	public static String sum(String stn1, String stn2) {
		StringBuilder result = new StringBuilder();
		
		int indexOfstn1 = stn1.length()-1;
		int indexOfstn2 = stn2.length()-1;
		
		int carry = 0;
		while(indexOfstn1 >= 0 || indexOfstn2 >= 0) {
			int digit1 = indexOfstn1 >=0 ? stn1.charAt(indexOfstn1) - '0': 0;
			int digit2 = indexOfstn2 >= 0 ? stn2.charAt(indexOfstn2) - '0': 0;
			int value = digit1 + digit2 + carry;
			
			carry = value > 9 ? 1 : 0;
			value = value % 10;
			result.append(value);
			
			indexOfstn1--;
			indexOfstn2--;
		}
		
		if(carry == 1) {
			result.append(1);
		}
		String resultString = result.reverse().toString();
		logger.info("Phép toán " + stn1 + " + " + stn2 + " = "+ resultString);
		return resultString;
	}
}
