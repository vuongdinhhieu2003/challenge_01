package test;

import org.junit.jupiter.api.Test;

import handleBigNum.MyBigNumber;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MyBigNumberTest {
	@Test
    public void testSumSimple() {
		assertEquals("5", MyBigNumber.sum("2", "3"));
        assertEquals("11111111100246913578", MyBigNumber.sum("1234567890123456789" ,"9876543210123456789"));
        assertEquals("0", MyBigNumber.sum("0" ,"0"));
        assertEquals("125", MyBigNumber.sum("0" ,"125"));
    }
}
