# Challenge_01: Add two large number

## Overview

This application demonstrates basic addition of arbitrarily large numbers using custom string manipulation.

## Setup
1. Prerequisites:

* Java Development Kit (JDK) 8 or later
* Maven
2. Clone the repository:

```bash
git clone git@gitlab.com:vuongdinhhieu2003/challenge_01.git
```
3. Build with Maven:

```Bash
cd Challenge_01
mvn clean install
```
4. Run the demo:

```Bash
java -cp target/Challenge_01-0.0.1-SNAPSHOT.jar handleBigNum.demo
```
## Usage

The application provides a sum method within the handleBigNum.MyBigNumber class for adding two large numbers represented as strings.

Example:
```java
String result = MyBigNumber.sum("1234567890123456789", "9876543210123456789");
System.out.println(result); // Output: 111111111100246913578
```
## Testing

Unit tests are located in the test package and can be run using Maven:

```Bash
mvn test
```
## Limitations

The current implementation doesn't handle negative numbers or decimals.
It might have limitations for extremely large numbers due to memory constraints.
## Potential Enhancements

Support for negative numbers and decimals.
Improve performance for very large numbers, potentially using specialized libraries like BigInteger.
Add input validation to prevent invalid inputs.
Explore alternative algorithms for efficient big number arithmetic